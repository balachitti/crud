const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let SalarySchema = new mongoose.Schema({
   emp_id: {
      type: String
   },
   Basic: {
      type: Number
   },
   HRA: {
      type: Number
   },
   PF: {
      type: Number
   },
   ESI: {
      type: Number
   },
   Bonus: {
      type: Number
   }
  

})

module.exports = mongoose.model('Salary', SalarySchema)