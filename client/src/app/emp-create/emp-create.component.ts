import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from '../employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-emp-create',
  templateUrl: './emp-create.component.html',
  styleUrls: ['./emp-create.component.css']
})
export class EmpCreateComponent implements OnInit {
  public submitted = false;
  employeeForm: FormGroup;
  serverErrorMessages: string;
  EmployeeProfile: any = ['engineer', 'sr.engineer', 'HR', 'TeamLeader', 'ProjectManager']
  constructor(
    public fb: FormBuilder,
    private router: Router,
    private employeeservice: EmployeeService,

  ) {
     this.mainForm();
    }

  ngOnInit() {}
  mainForm() {
    this.employeeForm = this.fb.group({
      _id : ['', [Validators.required, Validators.pattern(/(ACE[0-9]{4})$/)]],
      name : ['', [Validators.required, Validators.pattern(/([A-Za-z]+$)/)]],
      email : ['', [Validators.required, Validators.email]],
      designation : ['', [Validators.required]],
      phoneNumber : ['', [Validators.required, Validators.pattern(/[6-9]{1}[0-9]{9}$/)]]
    });
  }
  updateProfile(e) {
    this.employeeForm.get('designation').setValue(e, {
      onlySelf: true
    });
  }
  get myForm() {
    return this.employeeForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // console.log(this.submitted)
    if (!this.employeeForm.valid) {
      return false;
    } else {
      // this.employeeservice.createEmp(this.employeeForm.value);
      // console.log('Employee successfully created!');
      // this.router.navigateByUrl('/employees-list');
      this.employeeservice.createEmp(this.employeeForm.value).subscribe(
        (res) => {
          console.log('Employee successfully created!');
          this.router.navigateByUrl('/employees-list');
        },
        err => {
          if (err.status === 422) {
            this.serverErrorMessages = err.error.join('<br/>');
          }
        }
        );
  }
}
}
